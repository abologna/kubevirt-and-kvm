# NUMA pinning

KubeVirt doesn't currently implement NUMA pinning due to Kubernetes
limitation.

## Kubernetes Topology Manager

Allows aligning CPU and peripheral device allocations by NUMA node.

Many limitations:

* Not scheduler aware.
* Doesn’t allow memory alignment.
* etc...
